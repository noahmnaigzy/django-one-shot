from django.contrib import admin
from .models import TodoList
from .models import TodoItem


# Register your models here.
class TodoItemAdmin(admin.ModelAdmin):
    list_display = ("task", "due_date", "is_completed")


admin.site.register(TodoList)
admin.site.register(TodoItem)
